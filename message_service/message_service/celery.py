import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'message_service.settings')

app = Celery('message_service')
app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'Update mailing list': {
        'task': 'mailing_list.tasks.update_mailing_list',
        'schedule': 5.0,
    },
}
