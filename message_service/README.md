# Fabrique
Тестовое задание от Фабрики Решений


Комманды для запуска:
```
sudo apt install git pipenv
```

Создайте папку для проекта в папке и запустите терминал и введите комманду
```
git clone https://gitlab.com/nikitakopylov0023/Django-solution_factory.git

```
запустите виртуальную среду  
```
pipenv shell
```
Произойдёт запуск окружения и установка необходимых пакетов.
После этого по-очереди введите комманды
```python
python manage.py makemigrations
python manage.py migrate
```
Создайте админа.
```python
python manage.py createsuperuser
```
Запустите сервер командой
```python
python manage.py runserver
```

