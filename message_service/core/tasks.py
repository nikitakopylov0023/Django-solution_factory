import requests
from datetime import datetime
from django.conf import settings
from message_service.celery import app
from .models import MailingList, Message


@app.task
def send_mail(msg_id: int, text: str, end_datetime: datetime) -> None:
    message = Message.objects.get(id=msg_id)
    if datetime.strptime(end_datetime, '%Y-%m-%dT%H:%M:%SZ') > datetime.now():
        try:
            response = requests.get(
                f'http://probe.fbrq.cloud/v1/send/{message.id}/',
                headers={
                    'Authorization': f'Bearer {settings.MSG_API_TOKEN}',
                },
                json={
                    'id': message.client.id,
                    'phone': message.client.text,
                    'text': text,
                },
            )

            if response.status_code == 200:
                message.status = 1
            else:
                message.status = 2
                message.error_message = response.text
        except Exception as e:
            message.status = 2
            message.error_message = str(e)
    else:
        message.status = 2
        message.error_message = 'Out of time'

    message.save()
    print(f'Send {message.phone} {text}')


@app.task
def send_mailing_list(mailing_list_id: int) -> None:
    ml = MailingList.objects.get(
        id=mailing_list_id,
    )

    ml.status = 1
    ml.save()
    messages = ml.create_messages()

    for message in messages:
        send_mail.delay(
            message.id,
            message.mailing_list.text,
            message.mailing_list.end_datetime,
        )


@app.task
def update_mailing_list() -> None:
    ml_objects = MailingList.objects.filter(
        status=0,
        start_datetime__lte=datetime.now(),
    )

    for mailing_list in ml_objects:
        send_mailing_list.delay(
            mailing_list.id,
        )
