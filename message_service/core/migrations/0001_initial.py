# Generated by Django 4.0.6 on 2022-09-21 04:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(max_length=11, verbose_name='Номер телефона')),
                ('operator_code', models.CharField(max_length=50, verbose_name='Код мобильного оператора')),
                ('tag', models.CharField(max_length=50, verbose_name='Тег клиента')),
                ('utc_code', models.IntegerField(verbose_name='Код часового пояса')),
            ],
            options={
                'verbose_name': 'Клиент',
                'verbose_name_plural': 'Клиенты',
            },
        ),
        migrations.CreateModel(
            name='MailingList',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_datetime', models.DateTimeField(verbose_name='Дата и время запуска рассылки')),
                ('text', models.TextField(verbose_name='Текст сообщения')),
                ('client_filter_type', models.IntegerField(choices=[(0, 'All'), (1, 'Operator'), (2, 'Tag')], default=0, verbose_name='Тип фильтра свойств клиента')),
                ('client_filter_value', models.CharField(blank=True, max_length=50, null=True, verbose_name='Значение фильтра свойств клиента')),
                ('end_datetime', models.DateTimeField(verbose_name='Дата и время окончания рассылки')),
                ('status', models.IntegerField(choices=[(0, 'Created'), (1, 'Processed')], default=0, verbose_name='Статус рассылки')),
            ],
            options={
                'verbose_name': 'Рассылка',
                'verbose_name_plural': 'Рассылки',
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('create_datetime', models.DateTimeField(verbose_name='Дата и время создания')),
                ('status', models.IntegerField(choices=[(0, 'Created'), (1, 'Sended'), (2, 'Error')], default=0, verbose_name='Статус')),
                ('error_message', models.CharField(blank=True, max_length=100, null=True, verbose_name='Текст ошибки')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.client')),
                ('mailing_list', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.mailinglist')),
            ],
        ),
    ]
