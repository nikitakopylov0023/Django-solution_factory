from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.decorators import action
from .serializers import ClientSerializer, MailingListSerializer
from .models import Client, MailingList


class ClientViewSet(viewsets.ModelViewSet):
    """API для взаимодействия с клиентами."""
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = [permissions.AllowAny]


class MailingListViewSet(viewsets.ModelViewSet):
    """API для взаимодействия с рассылками."""
    queryset = MailingList.objects.all()
    serializer_class = MailingListSerializer
    permission_classes = [permissions.AllowAny]

    @action(methods=['get'], detail=True, permission_classes=[permissions.AllowAny])
    def stat(self, request, pk):
        ml = MailingList.objects.get(
            id=pk
        )
        return Response(
            ml.get_status(),
        )
