from pyexpat.errors import messages
from django.db import models
from datetime import datetime



class MailingList(models.Model):
    """Модель рассылки."""

    mailing_list_status = [
        (0, 'Created'),
        (1, 'Processed'),
    ]
    mailing_list_status_default = 0
    filter_type = [
        (0, 'All'),
        (1, 'Operator'),
        (2, 'Tag'),
    ]
    filter_type_default = 0
    start_datetime = models.DateTimeField(
        'Дата и время запуска рассылки',
    )
    text = models.TextField(
        'Текст сообщения',
    )
    client_filter_type = models.IntegerField(
        'Тип фильтра свойств клиента',
        choices=filter_type,
        default=filter_type_default,
    )
    client_filter_value = models.CharField(
        'Значение фильтра свойств клиента',
        max_length=50,
        blank=True,
        null=True,
    )
    end_datetime = models.DateTimeField(
        'Дата и время окончания рассылки',
    )
    status = models.IntegerField(
        'Статус рассылки',
        choices=mailing_list_status,
        default=mailing_list_status_default,
    )

    def _get_clients_by_filters(self):
        queryset = Client.objects.all()
        if self.client_filter_type == 1:
            queryset = queryset.filter(
                operator_code=self.client_filter_value,
            )
        elif self.client_filter_type == 2:
            queryset = queryset.filter(
                tag=self.client_filter_value,
            )

        return queryset

    def create_messages(self) -> list:
        clients = self._get_clients_by_filters()
        result_data = []

        for client in clients:
            result_data.append(
                Message.objects.create(
                    create_datetime=datetime.now(),
                    mailing_list=self,
                    client=client,
                )
            )

        return result_data

    def get_status(self):
        messages = Message.objects.filter(
            mailing_list=self,
        )
        all_count = len(messages)
        sended_count = len(messages.filter(
            status=1,
        ))
        error_count = len(messages.filter(
            status=2,
        ))

        return {
            'all': all_count,
            'sended': sended_count,
            'error': error_count,
        }

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


class Client(models.Model):
    """Модель клиента."""

    phone = models.CharField(
        'Номер телефона',
        max_length=11,
    )
    operator_code = models.CharField(
        'Код мобильного оператора',
        max_length=50,
    )
    tag = models.CharField(
        'Тег клиента',
        max_length=50,
    )
    utc_code = models.IntegerField(
        'Код часового пояса',
    )

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


class Message(models.Model):
    message_status = [
        (0, 'Created'),
        (1, 'Sended'),
        (2, 'Error'),
    ]
    message_status_default = 0
    create_datetime = models.DateTimeField(
        'Дата и время создания',
    )
    status = models.IntegerField(
        'Статус',
        choices=message_status,
        default=message_status_default,
    )
    error_message = models.CharField(
        'Текст ошибки',
        max_length=100,
        blank=True,
        null=True,
    )
    mailing_list = models.ForeignKey(
        MailingList,
        on_delete=models.CASCADE,
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
    )


