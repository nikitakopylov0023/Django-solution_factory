import re
from .models import Client, MailingList
from rest_framework import serializers


class ClientSerializer(serializers.ModelSerializer):

    def validate(self, data):
        result = re.match(
            r'^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$',
            data["phone"],
        )
        if not bool(result):
            raise serializers.ValidationError(
                'This field must be a 7XXXXXXXXXX.',
            )

        return data

    class Meta:
        model = Client
        fields = (
            'id',
            'phone',
            'operator_code',
            'tag',
            'utc_code',
        )
        read_only_fields = ('id',)


class MailingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = (
            'id',
            'start_datetime',
            'text',
            'client_filter_type',
            'client_filter_value',
            'end_datetime',
            'status',
        )
        read_only_fields = ('id',)