from django.urls import include, path
from rest_framework import routers

from .views import ClientViewSet, MailingListViewSet


router = routers.DefaultRouter()
router.register(r'clients', ClientViewSet)
router.register(r'mailing', MailingListViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
